package gui;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Label;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.util.Enumeration;
import java.util.Locale;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import dama.Dama;
import dama.Player;
import dama.io.Input;
import dama.io.NetworkSetup;
import dama.io.Output;
import dama.io.Turn;
import dama.operations.AI;
import dama.operations.Check;
import dama.operations.Moves;
import dama.state.Desk;
import dama.state.Figure;
import dama.state.Position;
import dama.state.Settings;



/**
 * Uzivatelske rozhrani pro hru Dama
 * @author Roman Luks
 * @author Pavel Hala
 *
 */
public class Gui extends JFrame
                               implements ActionListener, MouseListener {
    /**
     *
     */
    private static final long serialVersionUID = 7275553945417568909L;

    JDesktopPane desktop;

    //mys
    protected boolean mouseEnabled = true;
    protected static int clickCount = 0; //po dvou klicich posilame tah hre

    //nastaveni hry
    public Settings settings;

    //menu
    protected JMenu menu;
    protected JMenuItem menuItemSave;

    //vnitrni ramce
    protected Victory victory;
    protected Fail fail;
    protected LoadGame inFrLoadGame;
    protected NewGameSettings inFrNewGameSett;
    protected About about;
    public NewP2PGameSettings inFrNewP2PGameSett;

    //pultahy
    protected Turn turn;

    //texty s informacemi
    public JLabel lblInfo;
    protected JLabel lblPlayercolor;

    //napoveda
    protected JLabel LNapoveda;
    protected JButton BNapoveda;

    //pole labelu na obrazky  figurek
    protected JLabel[][] pole_figurek;
    protected JLabel[][] pole_napovedy;
    protected JLabel lblAura;    //oznaceni mysi
    protected JLabel lblRuna;    //figurky s mustTakeFlag = true

    //deska
    private int x_sachovnice = 40;
    private int x_offset = 5;
    private int y_sachovnice = 80;
    private int y_offset = 50;

    //notace
    protected JTextField TFPrikaz;
    protected TextArea TAVypisNotace;
    protected Button BSubmitCommand;

    //obrazky
    protected ImageIcon imIcAura;
    protected ImageIcon imIcSachovnice;

    protected ImageIcon imIcBilyPesak;
    protected ImageIcon imIcBilaDama;

    protected ImageIcon imIcCernyPesak;
    protected ImageIcon imIcCernaDama;
    protected ImageIcon imIcRuna;

    protected ImageIcon imIcWhiteBox;
    protected ImageIcon imIcBlackBox;

    protected ImageIcon imIcSeal;    //lachtan


///==============================================================================================================
///==============================================================================================================

    /**
     * Konstruktor hlavniho okna hry
     * @param insetx    vzdalenost v pixelech od leve hrany obrazovny
     * @param insety    vzdalenost v pixelech od horni hrany obrazovky
     */
    public Gui(int insetx, int insety)
    {
        super("Gui");

        //Nastaveni okna
        setBounds(insetx, insety, 800, 600);
        this.setResizable(false);

        //GUI
        desktop = new JDesktopPane();
        desktop.setBackground(Color.LIGHT_GRAY);
        setContentPane(desktop);

        desktop.setDragMode(JDesktopPane.LIVE_DRAG_MODE);

        this.setMouse(false);
        addMouseListener(this);

        //obrazky
        java.net.URL imageURL = PlayerGui.class.getResource("images/sachovnice_420px_yellow.png");
        if (imageURL != null) {this.imIcSachovnice = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/bily_pesak.png");
        if (imageURL != null) {this.imIcBilyPesak = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/bila_dama.png");
        if (imageURL != null) {this.imIcBilaDama = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/cerny_pesak.png");
        if (imageURL != null) {this.imIcCernyPesak = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/cerna_dama.png");
        if (imageURL != null) {this.imIcCernaDama = new ImageIcon(imageURL);}

        imageURL = PlayerGui.class.getResource("images/runa.png");
        if (imageURL != null) {this.imIcRuna = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/whitebox.png");
        if (imageURL != null) {this.imIcWhiteBox = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/blackbox.png");
        if (imageURL != null) {this.imIcBlackBox = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/seal.png");
        if (imageURL != null) {this.imIcSeal = new ImageIcon(imageURL);}
        imageURL = PlayerGui.class.getResource("images/aura.png");
        if (imageURL != null) {this.imIcAura = new ImageIcon(imageURL);}

        //menu
        setJMenuBar(createMenuBar());

        //texty s informacemi
        lblInfo = new JLabel("Not playing.");
        lblInfo.setHorizontalAlignment(SwingConstants.CENTER);
        desktop.setLayer(lblInfo, 1);
        lblInfo.setBounds(40, 10, 360, 30);
        desktop.add(lblInfo);

        this.lblPlayercolor = new JLabel("");
        this.lblPlayercolor.setBounds(25, 10, 45, 30);
        desktop.add(this.lblPlayercolor);

        //napoveda
        this.LNapoveda = new JLabel("No help requested...");
        LNapoveda.setHorizontalAlignment(SwingConstants.CENTER);
        this.LNapoveda.setBounds(502, 15, 260, 30);
        desktop.add(LNapoveda);

        this.BNapoveda = new JButton("Help!");
        this.BNapoveda.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent arg0)
            {
                if(Dama.isGameSetAndRunning())
                {
                    Desk deskForHelp = new Desk(Dama.getDesk());
                    char playerColor = Dama.getPlayerColor();
                    int aiStochasticity = 10;

                    Turn helpTurn = AI.AIMove(deskForHelp, playerColor, aiStochasticity);

                    if(helpTurn != null)
                    {
                        LNapoveda.setText("Marked figures can move!");
                        ZobrazNapovedu();
                    }
                    else LNapoveda.setText("Stalemate!");

                }
            }
        });
        this.BNapoveda.setBounds(422, 15, 70, 20);
        this.BNapoveda.setEnabled(false);
        desktop.add(this.BNapoveda);

        //deska
        JLabel BGSachovnice = new JLabel("");
        BGSachovnice.setFocusable(false);
        BGSachovnice.setIcon(this.imIcSachovnice);

        BGSachovnice.setBounds(x_sachovnice, y_sachovnice, 420, 420);
        BGSachovnice.addMouseListener(this);
        desktop.add(BGSachovnice);
        desktop.setLayer(BGSachovnice, -1);

        String columnsLegend = new String("    A       B      C      D      E      F      G      H");
        String rowsLegend = new String("8\r\n\r\n7\r\n\r\n6\r\n\r\n5\r\n\r\n4\r\n\r\n3\r\n\r\n2\r\n\r\n1");

        Label LXosaPopisNahore = new Label(columnsLegend);
        LXosaPopisNahore.setFont(new Font("Tahoma", Font.PLAIN, 20));
        LXosaPopisNahore.setBounds(40, 55, 420, 20);
        desktop.add(LXosaPopisNahore);

        Label LXosaPopisDole = new Label(columnsLegend);
        LXosaPopisDole.setFont(new Font("Tahoma", Font.PLAIN, 20));
        LXosaPopisDole.setBounds(40, 505, 420, 20);
        desktop.add(LXosaPopisDole);

        JTextPane TPYosaLeva = new JTextPane();
        TPYosaLeva.setEditable(false);
        TPYosaLeva.setBackground(Color.LIGHT_GRAY);
        TPYosaLeva.setFont(TPYosaLeva.getFont().deriveFont(20f));
        TPYosaLeva.setText(rowsLegend);
        TPYosaLeva.setBounds(17, 95, 17, 402);
        desktop.add(TPYosaLeva);

        JTextPane TPYosaPrava = new JTextPane();
        TPYosaPrava.setEditable(false);
        TPYosaPrava.setText(rowsLegend);
        TPYosaPrava.setFont(TPYosaPrava.getFont().deriveFont(20f));
        TPYosaPrava.setBackground(Color.LIGHT_GRAY);
        TPYosaPrava.setBounds(465, 95, 17, 402);
        desktop.add(TPYosaPrava);

        //aura
        this.lblAura = new JLabel("");
        desktop.add(this.lblAura);

        this.lblRuna = new JLabel("");
        desktop.setLayer(this.lblRuna, 3);
        desktop.add(this.lblRuna);

        //notace
        this.TAVypisNotace = new TextArea();
        TAVypisNotace.setEditable(false);
        this.TAVypisNotace.setBounds(535, 80, 200, 350);
        desktop.add(this.TAVypisNotace);

        this.BSubmitCommand = new Button("OK");
        BSubmitCommand.setEnabled(false);
        BSubmitCommand.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                String string = TFPrikaz.getText();
                string = string.toLowerCase(Locale.ENGLISH);
                TFPrikaz.setText("");
                if (!Check.isNotationValid(string))
                {
                    lblInfo.setText("Invalid move");
                    return;
                }

                Position pos1=Position.positionStrToPositionLowerCase(string.substring(0, 2));
                Position pos2=Position.positionStrToPositionLowerCase(string.substring(3, 5));
                char barva=Dama.getPlayerColor();
                //neni rozehrana hra, takze se nic delat nebude
                if (!( (barva=='B') || (barva=='W') )) return;
                if ( (pos1==null) || (pos2==null) )
                {
                    lblInfo.setText("Invalid move");
                    return;
                }

                Turn turn=new Turn(barva, pos1, pos2);
                Input.inputTurn(turn);
                return;
            }
        });
        this.BSubmitCommand.setBounds(670, 440, 65, 20);
        desktop.add(BSubmitCommand);

        this.TFPrikaz = new JTextField();
        this.TFPrikaz.setBounds(535, 440, 130, 20);
        desktop.add(this.TFPrikaz);
        this.TFPrikaz.setColumns(10);

        pole_figurek=new JLabel[8][8];
        pole_napovedy=new JLabel[8][8];
        for (int row=0; row<8; row++)
            for (int col=0; col<8; col++)
            {
                this.pole_figurek[row][col]=new JLabel("");
                this.pole_figurek[row][col].setBounds(x_sachovnice+10+(col*50), y_sachovnice+10+350-(row*50),50,50);
                desktop.add(this.pole_figurek[row][col]);

                this.pole_napovedy[row][col]=new JLabel("");
                this.pole_napovedy[row][col].setBounds(x_sachovnice+10+(col*50), y_sachovnice+10+350-(row*50),50,50);
                desktop.add(this.pole_napovedy[row][col]);
                desktop.setLayer(this.pole_napovedy[row][col], 3);
            }
    } ///konec konstruktoru

///===============================================================================================================
///===============================================================================================================

    /**
     * Vytvori menu
     * @return JMenuBar Menu obsahujici polozky New game, New P2P game, Load game, Save game, Player, About a Quit
     */
    protected JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        //Menu
        menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_D);
        menuBar.add(menu);

        //nova lokalni hra
        JMenuItem menuItem = new JMenuItem("New game");
        menuItem.setMnemonic(KeyEvent.VK_N);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_N, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("new");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        //nova sitova hra
        menuItem = new JMenuItem("New P2P game");
        menuItem.setMnemonic(KeyEvent.VK_W);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_W, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("newP2P");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        //nacteni ulozene hry
        menuItem = new JMenuItem("Load game");
        menuItem.setMnemonic(KeyEvent.VK_L);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_L, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("loadgame");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        //ulozeni probihajici hry
        menuItemSave = new JMenuItem("Save game");
        menuItemSave.setMnemonic(KeyEvent.VK_S);
        menuItemSave.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_S, ActionEvent.ALT_MASK));
        menuItemSave.setActionCommand("save");
        menuItemSave.addActionListener(this);
        menuItemSave.setEnabled(false);
        menu.add(menuItemSave);

        //prehravani partii
        menuItem = new JMenuItem("Player");
        menuItem.setMnemonic(KeyEvent.VK_P);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_P, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("player");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        //about
        menuItem = new JMenuItem("About");
        menuItem.setMnemonic(KeyEvent.VK_A);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_A, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("about");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        //quit
        menuItem = new JMenuItem("Quit");
        menuItem.setMnemonic(KeyEvent.VK_Q);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Q, ActionEvent.ALT_MASK));
        menuItem.setActionCommand("quit");
        menuItem.addActionListener(this);
        menu.add(menuItem);

        return menuBar;
    }

///===============================================================================================================

    //Reakce na menu a na tlacitka ve vnitrnich ramcich
    public void actionPerformed(ActionEvent e) {
        if ("new".equals(e.getActionCommand())) {                    //menu
            createNewGameSettingsFrame();
        }else if ("newP2P".equals(e.getActionCommand())){
            createNewP2PGameSettingsFrame();
        }else if ("loadgame".equals(e.getActionCommand())){
            createLoadGameFrame("Load game");
        }else if ("save".equals(e.getActionCommand())){
            createLoadGameFrame("Save game");
        }else if ("player".equals(e.getActionCommand())){
            Player.startingPlayerThread();
        }else if ("about".equals(e.getActionCommand())){
        	createAboutFrame();
        }else if ("quit".equals(e.getActionCommand())){
            quit();
        }else if ("startgame".equals(e.getActionCommand())){        //tlacitka ve vnitrnich ramcich
            startgame();
        }else if ("startP2Pgame".equals(e.getActionCommand())){
            NetworkSetup.startP2Pgame(this);
        }else if ("loadgamebutton".equals(e.getActionCommand())){
            LoadGameButton();
        }else if ("savegamebuttonXml".equals(e.getActionCommand())){
            SaveGameButtonXml();
        }else if("victoryOK".equals(e.getActionCommand())){
            NetworkSetup.killTheSockets(this.settings, true);
            this.setVisible(false);
            Dama.wakeUpGameThread();
            int x=this.getBounds().x;
            int y=this.getBounds().y;
            Dama.startingGameThread(x,y);
            this.dispose();
        }else if("failOK".equals(e.getActionCommand())){
            NetworkSetup.killTheSockets(this.settings, false);
            this.setVisible(false);
            Dama.wakeUpGameThread();
            int x=this.getBounds().x;
            int y=this.getBounds().y;
            Dama.startingGameThread(x,y);
            this.dispose();
        }else if ("savegamebuttonNot".equals(e.getActionCommand())){
            SaveGameButtonNot();
        }else{
            System.out.println("Neznama akce: " + e.getActionCommand());
        }
    }

///===============================================================================================================

    /**
     * Vytvori vnitrni ramec nastaveni lokalni hry
     */
    protected void createNewGameSettingsFrame() {

        this.inFrNewGameSett = new NewGameSettings();
        desktop.setLayer(inFrNewGameSett, 5);
        inFrNewGameSett.setLocation(110, 50);
        inFrNewGameSett.setVisible(true);

        JButton btnStartgame = new JButton("StartGame");
        inFrNewGameSett.getContentPane().add(btnStartgame);
        btnStartgame.setActionCommand("startgame");
        btnStartgame.addActionListener(this);

        desktop.add(inFrNewGameSett);
        try {
            inFrNewGameSett.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
    }

///===============================================================================================================

    /**
     * Vytvori vnitrni ramec nastaveni lokalni hry
     */
    protected void createAboutFrame() {

        this.about = new About();
        desktop.setLayer(about, 5);
        about.setLocation(110, 50);
        about.setVisible(true);

        //JButton btnStartgame = new JButton("StartGame");
        //inFrNewGameSett.getContentPane().add(btnStartgame);
        //btnStartgame.setActionCommand("startgame");
        //btnStartgame.addActionListener(this);

        desktop.add(about);
        try {
        	about.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
    }

///==
    /**
     * Vytvori vnitrni ramec pro nacitani drive ulozene hry
     * @param nazevOkna
     */
    protected void createLoadGameFrame(String nazevOkna)
    {
        this.inFrLoadGame=new LoadGame(nazevOkna);
        desktop.setLayer(inFrLoadGame, 5);
        inFrLoadGame.setLocation(100, 100);
        inFrLoadGame.setVisible(true);

        JButton btnLoadgame2;
        JButton btnLoadgame = new JButton("OK");
        btnLoadgame.setBounds(294, 39, 140, 20);

        inFrLoadGame.getContentPane().add(btnLoadgame);
        if (nazevOkna=="Load game")
        {
            btnLoadgame.setText("Load");
            btnLoadgame.setActionCommand("loadgamebutton");
        }
        else
        {
            btnLoadgame.setText("Save in XML");
            btnLoadgame.setActionCommand("savegamebuttonXml");

            btnLoadgame2 = new JButton("Save in notation");
            btnLoadgame2.setBounds(294, 60, 140, 20);
            btnLoadgame2.setActionCommand("savegamebuttonNot");
            btnLoadgame2.addActionListener(this);
            inFrLoadGame.getContentPane().add(btnLoadgame2);
        }
        btnLoadgame.addActionListener(this);

        desktop.add(inFrLoadGame);
        try{
            inFrLoadGame.setSelected(true);
        }catch (java.beans.PropertyVetoException e) {}
    }

///===============================================================================================================

    /**
     * Reakce na tlacitko XML v inFrLoadGame
     */
    public void SaveGameButtonXml()
    {
        String cesta=inFrLoadGame.GetPath();
        if (!cesta.endsWith(".xml")) cesta=cesta+".xml";
        inFrLoadGame.dispose();
        try
        {
            String xml = Output.getXmlFromSettingsAndTurnVector(Dama.getSettings(), Dama.getOutputTurns());
            Output.printStringToFile(xml, cesta);
        }
        catch (Exception e1)
        {
            //System.out.println("Nepodarilo se ulozit hru");
            this.createFail("Failed to save game in XML!");
            e1.printStackTrace();
        }
    }

///===============================================================================================================

    /**
     * Reakce na tlacitko Notace v inFrLoadGame
     */
    public void SaveGameButtonNot()
    {
        String notace=TAVypisNotace.getText();
        String cesta=inFrLoadGame.GetPath();
        if (!cesta.endsWith(".txt")) cesta=cesta+".txt";
        inFrLoadGame.dispose();
        try
        {
            Output.printStringToFile(notace, cesta);
        }
        catch (Exception e1)
        {
            this.createFail("Failed to save game in notation!");
            e1.printStackTrace();
        }
    }

///===============================================================================================================

    /**
     * Reakce na tlacitko v inFrLoadGame
     */
    public void LoadGameButton()
    {
        NetworkSetup.killTheSockets(this.settings, false);

        if(Dama.isGameSetAndRunning())
        {
            String cesta=inFrLoadGame.GetPath();
            inFrLoadGame.dispose();

            Dama.stopGameThread();
            Dama.interruptGameThread();
            int x=this.getBounds().x;
            int y=this.getBounds().y;
            this.dispose();
            Dama.startingGameThread(x,y);

            try {Thread.sleep(50);} catch (InterruptedException e) {e.printStackTrace();}

            if (cesta.endsWith(".xml")){
                if (!Input.XmlToVector(cesta, true, null)){
                    this.createFail("Nevalidni settings nebo tahy \nve vstupnim souboru XML nebo notaci");
                    return;
                }
            }else if (cesta.endsWith(".txt")){
                if (!Input.NotToVector(cesta, true, null)){
                    this.createFail("Nevalidni settings nebo tahy \nve vstupnim souboru XML nebo notaci");
                    return;
                }
            }else{
                this.createFail("Nevalidni settings nebo tahy \nve vstupnim souboru XML nebo notaci");
                return;
            }

            try {Thread.sleep(50);} catch (InterruptedException e) {e.printStackTrace();}
            Dama.startGame();
            Dama.wakeUpGameThread();
        }
        else
        {
            String cesta=inFrLoadGame.GetPath();
            inFrLoadGame.dispose();
            if (cesta.endsWith(".xml")){
                if (!Input.XmlToVector(cesta, true, null)){
                    this.createFail("Nevalidni settings nebo tahy \nve vstupnim souboru XML nebo notaci");
                    return;
                }
            }else if (cesta.endsWith(".txt")){
                if (!Input.NotToVector(cesta, true, null)){
                    this.createFail("Nevalidni settings nebo tahy \nve vstupnim souboru XML nebo notaci");
                    return;
                }
            }else{
                this.createFail("Nevalidni settings nebo tahy \nve vstupnim souboru XML nebo notaci");
                return;
            }
            Dama.startGame();
            Dama.wakeUpGameThread();
            this.menuItemSave.setEnabled(true);
            this.BSubmitCommand.setEnabled(true);
        }
    }

///===============================================================================================================

    /**
     * Vytvori vnitrni ramec nastaveni P2P hry
     */
    protected void createNewP2PGameSettingsFrame()
    {
        this.inFrNewP2PGameSett = new NewP2PGameSettings();
        desktop.setLayer(inFrNewP2PGameSett, 5);
        inFrNewP2PGameSett.setLocation(110, 50);
        inFrNewP2PGameSett.setVisible(true);

        JButton btnStartP2Pgame = new JButton("StartP2PGame");
        inFrNewP2PGameSett.getContentPane().add(btnStartP2Pgame);
        btnStartP2Pgame.setActionCommand("startP2Pgame");
        btnStartP2Pgame.addActionListener(this);

        desktop.add(inFrNewP2PGameSett);
        try {
            inFrNewP2PGameSett.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}
    }

///===============================================================================================================

    /**
     * Vytvori internal frame s upozornenim ze doslo k viteznemu stavu
     */
    public void createVictory(){

        this.victory = new Victory();
        desktop.setLayer(this.victory, 5);
        this.victory.setLocation(200, 200);
        this.victory.setVisible(true);

        JButton btnOK = new JButton("OK");
        this.victory.getContentPane().add(btnOK);
        btnOK.setActionCommand("victoryOK");
        btnOK.addActionListener(this);

        JButton btnSave = new JButton("Save game");
        this.victory.getContentPane().add(btnSave);
        btnSave.setActionCommand("save");
        btnSave.addActionListener(this);

        desktop.add(this.victory);
        try {
            this.victory.setSelected(true);
        } catch (java.beans.PropertyVetoException e) {}

    }

///===============================================================================================================

    /**
     * Zobrazi internal frame, ktere sdeluje ze doslo k chybe
     * @param string text pro textarea
     */
    public void createFail(String string)
    {

           this.fail = new Fail(string);
           desktop.setLayer(this.fail, 5);
           this.fail.setLocation(200, 100);
           this.fail.setVisible(true);

            JButton btnNewButton = new JButton("OK");
            GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
            gbc_btnNewButton.gridx = 0;
            gbc_btnNewButton.gridy = 2;
            this.fail.getContentPane().add(btnNewButton, gbc_btnNewButton);
            btnNewButton.setActionCommand("failOK");
            btnNewButton.addActionListener(this);

            desktop.add(this.fail);
            try {
                this.fail.setSelected(true);
            } catch (java.beans.PropertyVetoException e) {}

    }

///===============================================================================================================

    /**
     * Ziska settings z vnitrniho ramce inFrNewGameSett
     * vlozi nastaveni do vlakna hry a probudi ho a spusti hru
     */
    protected void startgame()
    {
        NetworkSetup.killTheSockets(this.settings, false);

        this.settings = this.inFrNewGameSett.getSettings();


        if(this.settings != null)
        {
            //System.out.println("GUI:Starting the game...");
            this.lblInfo.setText(this.settings.getSettingsString());


            if(Dama.isGameSetAndRunning())
            {
                Dama.interruptGameThread();
                Dama.stopGameThread();
                int x=this.getBounds().x;
                int y=this.getBounds().y;

                this.dispose();
                try {Thread.sleep(300);} catch (InterruptedException e) {e.printStackTrace();}
                Dama.startingGameThread(x,y);
                try {Thread.sleep(50);} catch (InterruptedException e) {e.printStackTrace();}
                Dama.setSettings(this.settings);
                try {Thread.sleep(50);} catch (InterruptedException e) {e.printStackTrace();}

                Dama.startGame();
                Dama.wakeUpGameThread();
            }
            else
            {
                //System.out.println("gui: ukladanim nastaveni do NOVE...");
                Dama.setSettings(this.settings);
                //System.out.println("gui: povoluji akce...");
                this.BSubmitCommand.setEnabled(true);
                this.BNapoveda.setEnabled(true);
                this.menuItemSave.setEnabled(true);

                Dama.startGame();
                Dama.wakeUpGameThread();
                this.inFrNewGameSett.setVisible(false);
                this.inFrNewGameSett.dispose();
            }
        }
        else
        {
            //System.out.println("Configure settings!");
            this.lblInfo.setText("Configure settings!");
        }
    }

///===============================================================================================================

    /**
     * Ukonceni hry tlacitkem v menu
     */
    protected void quit()
    {
        NetworkSetup.killTheSockets(this.settings, false);
        System.out.println("Quit v menu, ukoncuji...");
        System.exit(0);
    }

///===============================================================================================================


    /**
     * Stara funkce pro vytvoreni gui
     */
    public static void createAndShowGUI()
    {

        JFrame.setDefaultLookAndFeelDecorated(true);

        Gui frame = new Gui(50, 50);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);
    }

///===============================================================================================================

    /**
     * Povoluje a zakazuje kliknuti mysi
     * @param value boolean
     */
    public void setMouse(boolean value)
    {
        this.mouseEnabled = value;
    }

///===============================================================================================================

    //Zpracuje klik tlacitka a posle zvolenou pozici na desce dale
    public void mouseClicked(MouseEvent e)
    {
        if(this.mouseEnabled){
            switch(e.getModifiers())
            {
                case InputEvent.BUTTON1_MASK:
                {
                    //System.out.println("That's the LEFT button");
                    Point p = getLocationOnScreen();
                    PointerInfo a = MouseInfo.getPointerInfo();
                    Point b = a.getLocation();

                    int x = (int) b.getX() - p.x - x_offset;
                    int y = (int) b.getY() - p.y - y_offset;
                    if ( (x>=(x_sachovnice+10)) && (y>=y_sachovnice+11) && (x<=(x_sachovnice+409)) && (y<=(y_sachovnice+410)) )
                    {
                        int sx = (x - 10 - x_sachovnice);
                        int sy = y - 11 - y_sachovnice;
                        sx = sx/50;
                        sy = 7 - sy/50;
                        //System.out.println(sx + ":" + sy);
                        Position pos = new Position(sy, sx);
                        this.mouseInput(pos);

                        this.lblAura.setIcon(this.imIcAura);
                        this.lblAura.setBounds(x_sachovnice+10+(sx*50), y_sachovnice+10+350-(sy*50),50,50);

                    }
                    break;
                }
                case InputEvent.BUTTON2_MASK:
                {
                    //System.out.println("That's the MIDDLE button");
                    break;
                }
                case InputEvent.BUTTON3_MASK:
                {
                    //System.out.println("That's the RIGHT button");
                    Gui.setClickCount(0);
                    this.clearAura();
                    break;
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {}
    @Override
    public void mouseExited(MouseEvent arg0) {}
    @Override
    public void mouseReleased(MouseEvent arg0) {}
    @Override
    public void mousePressed(MouseEvent arg0) {}

///===============================================================================================================

    /**
     * Zpracuje vstup od mysi (kliky na sachovnici) a posila ho dal
     * @param pos
     */
    public void mouseInput(Position pos)
    {
        if(Dama.isGameSetAndRunning())
        {
            Gui.clickCount++;
            //System.out.println("mouseInput:" + pos.getStr());

            if(Gui.clickCount == 1)
            {
                this.turn = new Turn(Dama.getTurn());
                this.turn.origin = new Position(pos);
            }
            if(Gui.clickCount == 2)
            {
                this.turn.destination = new Position(pos);
                Input.inputTurn(this.turn);
                Gui.clickCount = 0;
                this.clearAura();
            }
        }
    }

///===============================================================================================================

    /**
     * Nastavi pocet kliku
     * @param count    cislo na ktere se ma pocet kliku nastavit
     */
    public static void setClickCount(int count)
    {
        Gui.clickCount = count;
        //System.out.println("setClickCount:" + count);
    }

///===============================================================================================================

    /**
     * Vykresli stav desky
     * @param desk
     */
    public void updateDesk(Desk desk)
    {
        this.lblInfo.setText("Refreshing playboard, please wait...");
        if (!Check.isMustTake(desk, Dama.getPlayerColor())) this.lblRuna.setIcon(null);
        SmazNapovedu();

        Figure[][] this_board=desk.getState();
        for (int row=0; row<8; row++)
        {
            for (int col=0; col<8; col++)
            {
                if (this_board[row][col]==null)
                {
                    this.pole_figurek[row][col].setIcon(null);
                    continue;
                }
                if (this_board[row][col].color=='B')
                {
                    if (this_board[row][col].type=='P')
                        this.pole_figurek[row][col].setIcon(this.imIcCernyPesak);
                    else
                        this.pole_figurek[row][col].setIcon(this.imIcCernaDama);
                }
                else if (this_board[row][col].color=='W')
                {
                    if (this_board[row][col].type=='P')
                        this.pole_figurek[row][col].setIcon(this.imIcBilyPesak);
                    else
                        this.pole_figurek[row][col].setIcon(this.imIcBilaDama);
                }

                if(this_board[row][col].mustTakeFlag == true)
                {
                    this.lblRuna.setIcon(this.imIcRuna);
                    this.lblRuna.setBounds(x_sachovnice+10+(col*50), y_sachovnice+10+350-(row*50),50,50);
                    desktop.add(this.lblRuna);
                }
            }
        }

        desk.clearMustTakeFlag();
        String barva;
        char playerColor = Dama.getPlayerColor();
        if (playerColor=='W') barva="WHITE's";
        else barva="BLACK's";
        this.lblInfo.setText("Playboard is ready for " + barva + " turn.");
        ImageIcon colorbox = (playerColor=='W') ? this.imIcWhiteBox : this.imIcBlackBox;
        this.lblPlayercolor.setIcon(colorbox);

    }

///===============================================================================================================

    /**
     * Zobrazi na desce hry napovedu. Pokud dany hrac nemusi brat, vsechny figurky co se muzou pohnout jsou modre oznaceny.
     * Pokud musi dany hrac brat, cervene se mu oznaci vsechny figurky co mohou brat.
     */
    public void ZobrazNapovedu()
    {
        Desk desk = Dama.getDesk();
        char natahu=Dama.getPlayerColor();
        Figure[][] this_board=desk.getState();
        boolean brani=Check.isMustTake(desk, natahu);
        desk.clearMustTakeFlag();

        for(int row=0; row<8; row++)
            for(int col=0; col<8; col++)
                if ((this_board[row][col]!=null) && (this_board[row][col].color==natahu))
                {
                    Position pos=new Position(row, col);
                    if ((brani)&&(Moves.canTake(pos, desk)))
                        this.pole_napovedy[row][col].setIcon(imIcRuna);
                    else if ((Moves.canMove(pos, desk))&&(!brani))
                        this.pole_napovedy[row][col].setIcon(imIcSeal);

                }
    }

///===============================================================================================================

    /**
     * Na desce smaze vizualni napovedu (modre a cervene oznaceni figurek).
     */
    public void SmazNapovedu()
    {
        for(int row=0; row<8; row++)
            for(int col=0; col<8; col++)
                this.pole_napovedy[row][col].setIcon(null);
    }

///===============================================================================================================

    /**
     * Fce prida to textarea dalsi tah v notaci
     * @param wturn    tah bileho
     * @param wtake    indikator jestli bily bral
     * @param bturn    tah cerneho
     * @param btake    indikator jestli cerny bral
     * @param tah      poradove cislo tahu
     */
    public void NotationNewLine(Turn wturn, boolean wtake, Turn bturn, boolean btake, int tah)
    {
        String predchozi_obsah=TAVypisNotace.getText();

        String p0=wturn.origin.getStrLowerCase();
        String p1=wturn.destination.getStrLowerCase();
        String w;
        if (wtake) w="x";
        else w="-";

        String p2=bturn.origin.getStrLowerCase();
        String p3=bturn.destination.getStrLowerCase();
        String b;
        if (btake) b="x";
        else b="-";

        TAVypisNotace.setText(predchozi_obsah + tah + ". " + p0 + w + p1 + " " + p2 + b + p3 + "\n");
    }

///===============================================================================================================

    /**
     * Fce prida to textarea tah bileho v notaci
     * @param wturn    tah bileho
     * @param wtake    indikator jestli bily bral
     * @param tah      poradove cislo tahu
     */
    public void NotationNewLineLastWhite(Turn wturn, boolean wtake, int tah)
    {
        String predchozi_obsah=TAVypisNotace.getText();

        String p0=wturn.origin.getStrLowerCase();
        String p1=wturn.destination.getStrLowerCase();
        String w;
        if (wtake) w="x";
        else w="-";

        TAVypisNotace.setText(predchozi_obsah + tah + ". " + p0 + w + p1);
    }

///===============================================================================================================

    /**
     * Fce prida to textarea tah cerneho v notaci
     * pouziti: cerny tahnul
     * @param bturn    tah cerneho
     * @param btake    indikator jestli cerny bral
     */
    public void NotationNewLineLastBlack(Turn bturn, boolean btake)
    {
        String predchozi_obsah=TAVypisNotace.getText();

        String p0=bturn.origin.getStrLowerCase();
        String p1=bturn.destination.getStrLowerCase();
        String w;
        if (btake) w="x";
        else w="-";

        TAVypisNotace.setText(predchozi_obsah + " " + p0 + w + p1 + "\n");
    }

///===============================================================================================================

    /**
     * Toto by melo kompletne zabit okno aplikace.
     */
    public void pullThePlug()
    {
        WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
    }

///===============================================================================================================

    /**
     * Nastavi label "lblInfo" aby zobrazoval zadany text.
     * @param text    text ktery chceme aby se zobrazoval v labelu
     */
    public void setInfoText(String text){
        this.lblInfo.setText(text);
    }

///===============================================================================================================
    /**
     * Vycisti hraci desku od zeleneho oznaceni kliknuti
     */
    public void clearAura(){
        this.lblAura.setIcon(null);
    }
///===============================================================================================================
    /**
     * Label s napovedou da do puvodniho stavu.
     */
    public void clearHelp(){
        this.LNapoveda.setText("No help requested...");
    }
///===============================================================================================================
    /**
     * Nastavi menu na (ne)klikatelne
     * @param value    boolean urcujici jestli bude enabled nebo disabled
     */
    public void setMenu(boolean value){
        this.menu.setEnabled(value);
    }
///===============================================================================================================
    /**
     * Nastavi tlacitko help na (ne)klikatelne
     * @param value    boolean urcujici jestli bude enabled nebo disabled
     */
    public void setHelpButton(boolean value){
        this.BNapoveda.setEnabled(value);
    }
///===============================================================================================================
    /**
     * Nastavi tlacitko submit(command inn notation) na (ne)klikatelne
     * @param value    boolean urcujici jestli bude enabled nebo disabled
     */
    public void setSubmitButton(boolean value){
        this.BSubmitCommand.setEnabled(value);
    }
///===============================================================================================================
    /**
     * Nastavi tlacitko save v menu na (ne)klikatelne
     * @param value    boolean urcujici jestli bude enable nebo disabled
     */
    public void setMenuItemSave(boolean value){
        this.menuItemSave.setEnabled(value);
    }

///===============================================================================================================

    /**
     * Pomocna funkce na zisk zvolenych radio buttonu
     * @param group skupina radio buttonu
     * @return zvoleny radio button
     */
    public static JRadioButton getSelection(ButtonGroup group)
    {
        for (Enumeration<AbstractButton> e = group.getElements(); e.hasMoreElements();)
        {
          JRadioButton b = (JRadioButton) e.nextElement();
          if (b.getModel() == group.getSelection()) {
            return b;
          }
        }
        return null;
    }
}
